Chat-Client
==============
Приложение состоит из:

#####Терминала
Запускает консоль для аутентификации и ввода сообщении
######Параметры
1. хост сервера
2. порт сервера
3. порт сокета к которому будет присоединяться Монитор  
java -cp chat-client-1.0-SNAPSHOT-jar-with-dependencies.jar ru.kalinin.chat.client.ui.ConsoleChatTerminal 127.0.0.1 8080 4045

#####Монитор
Присоединяется к терминалу и отбражает сообщения от других пользователей
######Параметры
1. порт монитора  
java -cp chat-client-1.0-SNAPSHOT-jar-with-dependencies.jar ru.kalinin.chat.client.ui.ConsoleChatMonitor 4045

#####Нагрузочное тестирование
Симулирует работу других пользователей
######Параметры
1. хост сервера
2. порт сервера
3. количество пользователей
4. время тестирования в минутах
java -cp chat-client-1.0-SNAPSHOT-jar-with-dependencies.jar ru.kalinin.chat.client.ui.ConsoleChatHightLoad 127.0.0.1 8080 1000 15
