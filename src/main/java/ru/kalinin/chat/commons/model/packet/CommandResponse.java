package ru.kalinin.chat.commons.model.packet;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by kalinin on 07.10.2017.
 */
@Getter @Setter
public class CommandResponse extends Packet implements Serializable {

    private Status status;
    private String body;

    public CommandResponse() {
    }

    @Override
    public String getTypePacket() {
        return "commandresponse";
    }


    public static enum Status {
        SUCCESS, ERROR, NOT_FOUND;
    }
}
