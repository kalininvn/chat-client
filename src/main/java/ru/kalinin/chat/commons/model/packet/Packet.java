package ru.kalinin.chat.commons.model.packet;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kalinin on 04.10.2017.
 */
@Getter
@Setter
public abstract class Packet implements Serializable {

    private static final long serialVersionUID = -4570062368833986967L;

    private String to;
    private String from;
    private Date date = new Date();
    public abstract String getTypePacket();
}
