package ru.kalinin.chat.client.model;

import lombok.Getter;
import lombok.Setter;
import ru.kalinin.chat.client.connection.AmqpConfiguration;
import ru.kalinin.chat.commons.model.packet.Message;

import java.util.List;

/**
 * Created by kalinin on 29.09.2017.
 */
@Getter @Setter
public class UserInfo {

    private String username;
    private AmqpConfiguration amqpConfiguration;
}
