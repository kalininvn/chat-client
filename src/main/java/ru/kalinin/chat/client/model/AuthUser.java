package ru.kalinin.chat.client.model;

import lombok.*;

/**
 * Created by kalinin on 06.10.2017.
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthUser {
    private String username;
}
