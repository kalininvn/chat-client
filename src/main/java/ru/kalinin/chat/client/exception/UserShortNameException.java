package ru.kalinin.chat.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 10.10.2017.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN)
public class UserShortNameException extends UserValidationException {

    public UserShortNameException() {
        super("Имя не должно быть меньше 3 символов");
    }

    public UserShortNameException(String message) {
        super(message);
    }
}
