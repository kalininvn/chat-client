package ru.kalinin.chat.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 10.10.2017.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN)
public class UserLongNameException extends UserValidationException {

    public UserLongNameException() {
        super("Имя не должно превышать 20 символов");
    }

    public UserLongNameException(String message) {
        super(message);
    }
}
