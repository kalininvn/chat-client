package ru.kalinin.chat.client.exception;

/**
 * Created by kalinin on 09.10.2017.
 */
public class ConnectNotInitializedException extends RuntimeException {

    public ConnectNotInitializedException() {
        super();
    }

    public ConnectNotInitializedException(String message) {
        super(message);
    }
}
