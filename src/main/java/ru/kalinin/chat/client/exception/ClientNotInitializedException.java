package ru.kalinin.chat.client.exception;

/**
 * Created by kalinin on 09.10.2017.
 */
public class ClientNotInitializedException extends RuntimeException {

    public ClientNotInitializedException() {
        super();
    }

    public ClientNotInitializedException(String message) {
        super(message);
    }
}
