package ru.kalinin.chat.client.ui;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.kalinin.chat.client.Messager;
import ru.kalinin.chat.client.exception.UserValidationException;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.nio.channels.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/**
 * Created by kalinin on 09.10.2017.
 */
@Slf4j
public class ConsoleChatTerminal {

    private final Selector selector;
    private final ServerSocketChannel ssc;
    private byte[] buffer = new byte[2048];
    private CharBuffer cb = CharBuffer.allocate(2048);
    private Charset ch = Charset.forName("UTF-8");
    private CharsetDecoder decoder = ch.newDecoder();
    //Мапа, где хранятся все SelectionKey и связанные с ним ByteBuffer для рассылки
    private Map<SelectionKey, ByteBuffer> connections = new HashMap<SelectionKey, ByteBuffer>();
    private Messager messager = null;
    private FakeMonitorRunnable fakeMonitorRunnable = null;
    private Thread fakeMonitorThread = null;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private Messager.ResponseHandler messageHandler = new Messager.ResponseHandler() {

        @Override
        public void onResponse(String from, Date dt, String message) {
            try {
                Writer writer = fakeMonitorRunnable.getWriter();
                writer.write(dateFormat.format(dt) + " сообщение от " + from + ": " + message); //пишем строку пользователя
                writer.write("\n"); //пишем строку пользователя
                writer.flush(); // отправляем
            } catch (IOException e) {
                e.printStackTrace();
                shutdownTerminal();
            }
        }
    };

    private Messager.ResponseHandler messageHandlerTerminal = new Messager.ResponseHandler() {

        @Override
        public void onResponse(String from, Date dt, String message) {

            try {
                System.out.println(dateFormat.format(dt) + " сообщение от " + from + ": " + message); //пишем строку пользователя
                System.out.println("Введите сообщение: ");
            } catch (Exception e) {
                e.printStackTrace();
                shutdownTerminal();
            }
        }
    };

    private Messager.ResponseHandler messageHandlerError = new Messager.ResponseHandler() {

        @Override
        public void onResponse(String from, Date dt, String message) {

            try {
                System.out.println(dateFormat.format(dt) + " сообщение от " + from + ": " + message); //пишем строку пользователя
            } catch (Exception e) {
                e.printStackTrace();
                shutdownTerminal();
            }
        }
    };

    @Getter @Setter
    private class FakeMonitorRunnable implements Runnable {

        private BufferedWriter socketWriter;

        public FakeMonitorRunnable(Integer portTerminal) throws IOException {
            final Socket s = new Socket("localhost", portTerminal); // создаем сокет
            // буферизированный писатель на терминал
            socketWriter = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));
        }

        private Writer getWriter() {
            return socketWriter;
        }

        @Override
        public void run() {

            try {
                System.out.println("Введите имя пользователя: ");
                Scanner in = new Scanner(System.in);
                while (true) {
                    String username = in.nextLine().toLowerCase();
                    try {
                        messager.start(username);
                        System.out.println("Успешно, можно общаться");
                        break;
                    } catch (UserValidationException e) {
                        System.out.println("Ошибка \"" + e.getMessage() + "\", введите другое имя: ");
                    } catch (Exception e) {
                        System.out.println("Критическая ошибка \"" + e.getMessage() + "\"");
                        throw e;
                    }
                }

                while (true) {
                    System.out.println("Введите сообщение: ");
                    String message = in.nextLine().toLowerCase();
                    try {
                        if(message.trim().equals("/shutdown")) {
                            shutdownTerminal();
                            break;
                        }
                        messager.sendMessage(message);
                    } catch (Exception e) {
                        throw e;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                shutdownTerminal();
            }
        }
    }

    /**
     * Конструктор объекта терминала
     * @param portTerminal Порт, где будем слушать входящие сообщения.
     * @throws IOException Если не удасться создать терминал-сокет, вылетит по эксепшену, объект Терминала не будет создан
     */
    public ConsoleChatTerminal(String hostServer, Integer portServer, int portTerminal) throws Exception {

        try {

            messager = new Messager(hostServer, portServer);
            messager.setHandlerResponseMessage(messageHandler);
            messager.setHandlerResponseTerminal(messageHandlerTerminal);
            messager.setHandlerResponseError(messageHandlerError);


            ssc = ServerSocketChannel.open(); // создаем терминалСокет канал
            ssc.configureBlocking(false); // отключаем режим блокирования в ожидании
            ssc.socket().bind(new InetSocketAddress(portTerminal)); // получаем обычный терминалсокет, который биндиться на нужный порт
            selector = Selector.open(); // создаем селектор прослушки
            ssc.register(selector, SelectionKey.OP_ACCEPT); // регистрируемся на селекторе на терминал-канал.

            createFakeMonitor(portTerminal);
        } catch (Exception e) {
            shutdownTerminal();
            throw e;
        }


    }

    private void createFakeMonitor(Integer portTerminal) throws IOException {

        fakeMonitorRunnable = new FakeMonitorRunnable(portTerminal);
        fakeMonitorThread = new Thread(fakeMonitorRunnable, "fakeMonitorThread-" + portTerminal);
        fakeMonitorThread.start();
    }

    /**
     * главный цикл прослушивания/ожидания коннекта.
     */
    public void run() {
        while (true) { //бесконечный цикл, типа...
            try {
                if (selector.isOpen()) {
                    selector.select();  // останавливаемся на ожиданни события от любого из подписанных каналов.
                    Set<SelectionKey> keys = selector.selectedKeys(); // получаем набор ключей (обычно - один)
                    for (SelectionKey sk:keys) {
                        if (!sk.isValid()) {
                            continue;
                        }
                        if (sk.isAcceptable()) { // если к нам коннект...
                            ServerSocketChannel ssca = (ServerSocketChannel)sk.channel(); // (ssca == ssc, кстати)
                            SocketChannel sc = ssca.accept(); // так как точно известно, что ожидает коннект - тут мы без остановки
                            sc.configureBlocking(false); // отключаем режим блокирования
                            // подписываемся только на события прихода данных
                            SelectionKey skr = sc.register(selector, SelectionKey.OP_READ);
                            ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
                            connections.put(skr, byteBuffer);
                            //q.offer(skr); // и ложим в очередь рассылки.
                        } else if (sk.isReadable()) { // если к нам посылка...
                            SocketChannel socketChannel= (SocketChannel)sk.channel(); // хватаем канал коннекта
                            int read;
                            ByteBuffer byteBuffer = connections.get(sk);
                            byteBuffer.clear(); // очищаем байт буфер (кто не успел записаться - тот опоздал)
                            try {
                                read = socketChannel.read(byteBuffer); // пробуем заполнить буфер
                            } catch (IOException e) { // беда, коннект отпал...
                                closeChannel(sk); // закрываем нафик
                                break; // выходим с цикла
                            }
                            if (read == -1) { // коннект отвалился в штатном режиме
                                closeChannel(sk); // тоже закрываем нафик
                                break;
                            } else if (read > 0) { // если что-то прочитали из сокета и записали в буфер...
                                byteBuffer.flip(); // готовим буфер для чтения.
//                                byteBuffer.mark(); // ставим метку (ибо декодер нам сломает состояние буффера)
//                                if (decodeAndCheck(read, byteBuffer)) break; // если декодер сказал - "выключаем терминал", прерываем цикл
//                                byteBuffer.reset(); // если не выключаем терминал - то возвращаемся на метку (кстати, это всегда 0) :)
                                final int pos = byteBuffer.position(); // запоминаем для быстрого проставления у остальных буфферов
                                final int lim = byteBuffer.limit();
                                // получаем сет наборов "ключ-его байтбуффер"
                                Set <Map.Entry<SelectionKey, ByteBuffer>> entries = connections.entrySet();
                                for (Map.Entry<SelectionKey, ByteBuffer> entry: entries) { //цикл по наборам
                                    SelectionKey selectionKey = entry.getKey(); // получаем ключ из набора
                                    selectionKey.interestOps(SelectionKey.OP_WRITE); //переключам в режим "хочу писать!"
                                    ByteBuffer entryBuffer = entry.getValue(); //получаем байт-буффер из набора
                                    entryBuffer.position(pos); // настраиваем его, чтобы правильно записать в сокет.
                                    entryBuffer.limit(lim);
                                }
                            }
                        } else if (sk.isWritable()) { // сообщение о готовности сокета для записи
                            ByteBuffer bb = connections.get(sk); // получаем байт-буффер, ассоциированный с ключем
                            SocketChannel s = (SocketChannel)sk.channel(); // выдергиваем канал
                            try {
                                int result = s.write(bb); // пробуем записать
                                if (result == -1) { // socket properly closed
                                    closeChannel(sk); // ну понятно, закрываем коннект
                                }
                            } catch (IOException e2) { // а это если отвал произошел в моментзаписи
                                closeChannel(sk); // тоже закрываем
                            }
                            if (bb.position() == bb.limit()) {
                                sk.interestOps(SelectionKey.OP_READ); //сразу ключ переключаем в режим "хотим читать!"
                            }
                        }
                    }
                    keys.clear(); // очищаем сет ключей, мы по идее обработали все.
                } else {
                    break;
                };
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        shutdownTerminal();
    }


    /**
     * Декодер - декодирует поток байтов и превращает в поток символов. Вообще, по сути, было проще
     * превратить команду shutdown в массив байтов и сравнивать по-байтно, но хотелось показать, как
     * декодировать.
     * @param read - сколько байтов прочитали из сокета.
     * @param ba   - ByteBuffer to decode
     * @return true - терминал выключен
     */
//    private boolean decodeAndCheck(int read, ByteBuffer ba) {
//        cb.clear(); // очищаем CharBuffer перед декодированием...
//        // это декодирование, не полное, надо признать, ибо нас интересует лишь стартовая фраза.
//        decoder.decode(ba, cb, false);
//        cb.flip(); // в чар буфер свалена пришедшая строка после декодирования
//        if ("shutdown\n".equals(cb.toString())) { //проверяем, что это "shutdown"
//            shutdownTerminal(); // если да - выключаем терминал
//            return true;
//        }
//        return false;
//    }

    /**
     * Метод закрывает канал сокета, снимает со списка активных ключей и удаляет из списка рассылки
     * @param sk - ключ, связанный с каналом
     * @throws IOException - если при закрытии прошла ошибка
     */
    private void closeChannel(SelectionKey sk) throws IOException {
        connections.remove(sk); // удаляем из списка рассылки
        SocketChannel socketChannel = (SocketChannel)sk.channel();
        if (socketChannel.isConnected()) {
            socketChannel.close(); // закрываем канал
        }
        sk.cancel(); // удаляем из списка селектора
    }


    /**
     * метод "глушения" терминала
     */
    private synchronized void shutdownTerminal() {
        // обрабатываем список рабочих коннектов, закрываем каждый
        Set<SelectionKey> skSet = connections.keySet();
        for (SelectionKey sq:skSet) {
            SocketChannel s = (SocketChannel)sq.channel();
            if (s.isConnected()) {
                try {
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (ssc.isOpen()) {
            try {
                ssc.close();
                selector.close();
            } catch (IOException ignored) {}
        }
        if(messager != null) {
            try {
                messager.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * входная точка программы
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws Exception {
        if(args.length != 3) {
            throw new IllegalArgumentException("Не верно заданы параметры, формат \"[хост сервера] [порт сервера] [порт сокета для монитора] \"");
        }
        String hostServer = args[0];
        Integer portServer = Integer.valueOf(args[1]);
        Integer portTerminal = Integer.valueOf(args[2]);


        new ConsoleChatTerminal(hostServer, portServer, portTerminal).run(); // если терминал не создался, программа
        // вылетит по эксепшену, и метод run() не запуститься
    }
}
