package ru.kalinin.chat.client.ui;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import ru.kalinin.chat.client.ChatClient;
import ru.kalinin.chat.commons.model.packet.CommandResponse;
import ru.kalinin.chat.commons.model.packet.Error;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.commons.model.packet.Presence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kalinin on 12.10.2017.
 */
public class ConsoleChatHightLoad {

    public static void main(String[] args) throws InterruptedException {

        if(args.length != 4) {
            throw new IllegalArgumentException("Не верно заданы параметры, формат \"[хост сервера] [порт сервера] [количество пользователей] [время тестирования в минутах] \"");
        }
        String hostServer = args[0];
        Integer portServer = Integer.valueOf(args[1]);
        int countUser = Integer.valueOf(args[2]);
        int timeTest = Integer.valueOf(args[3]);
        List<ChatClient> clients = Collections.synchronizedList(new ArrayList<ChatClient>());

        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("Client-%d")
                .setDaemon(false)
                .build();

        ExecutorService executorService = Executors.newFixedThreadPool(countUser, threadFactory);
        AtomicInteger ai = new AtomicInteger();
        AtomicInteger countMessage = new AtomicInteger();
        for (int i = 0; i < countUser; i++) {
            Thread.sleep(500);
            executorService.submit(() -> {
                    String username = "bot" + ai.incrementAndGet();
                    System.out.println("поток " + Thread.currentThread().getName() + " пользователь " + username + " " + new Date());
                    ChatClient chatClient = createChatClient(hostServer, portServer);
                    clients.add(chatClient);
                    chatClient.connect(username);
                    while (!Thread.currentThread().isInterrupted()) {
                        try {
                            Thread.sleep(15 * 1000);
                            int messageNumber = countMessage.incrementAndGet();
                            System.out.println("поток " + Thread.currentThread().getName() + " пользователь " + username + " " + new Date() + " Сообщение_" + messageNumber);
                            chatClient.sendMessage(null, "Сообщение_" + messageNumber);
                        } catch (InterruptedException e) {
                            System.out.println("поток прерван " + Thread.currentThread().getName() + " пользователь " + username + " " + new Date());
                            Thread.currentThread().interrupt();
                        }
                    }

            });
//            createChatClient(hostServer, portServer, "bot" + (i+1));
        }
        Thread.sleep(timeTest * 60 * 1000);
        executorService.shutdownNow();
    }

    private static ChatClient createChatClient(String host, int port) {

        ChatClient chatClient = new ChatClient(host, port) {

            @Override
            protected void processError(Error error) {

            }

            @Override
            protected void processCommand(CommandResponse commandResponse) {

            }

            @Override
            protected void processPresence(Presence presence) {

            }

            @Override
            protected void processMessage(Message message) {

            }
        };
        return chatClient;
    }
}
