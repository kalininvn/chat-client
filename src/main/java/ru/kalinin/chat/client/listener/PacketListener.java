package ru.kalinin.chat.client.listener;

import ru.kalinin.chat.commons.model.packet.Packet;

/**
 * Created by kalinin on 04.10.2017.
 */
public interface PacketListener {

    void processPacket(Packet packet);
}
