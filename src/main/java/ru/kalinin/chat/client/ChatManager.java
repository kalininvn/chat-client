package ru.kalinin.chat.client;

import ru.kalinin.chat.client.connection.Connection;
import ru.kalinin.chat.commons.model.packet.Command;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.commons.model.packet.Packet;

/**
 * Created by kalinin on 04.10.2017.
 */
public class ChatManager {

    private Connection connection;

    public ChatManager(Connection connection) {
        this.connection = connection;
    }

    public Message createMessage() {
        return new Message(Message.Type.GROUPCHAT);
    }

    public Command createCommand(String code, String... arg) {
        return new Command(code, arg);
    }

    void sendPacket(Packet packet) {

        if(packet.getFrom() == null) {
            packet.setFrom(this.connection.getUser());
        }

        this.connection.sendPacket(packet);
    }
}
