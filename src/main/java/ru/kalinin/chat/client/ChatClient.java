package ru.kalinin.chat.client;


import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import ru.kalinin.chat.client.connection.AmqpConnection;
import ru.kalinin.chat.client.connection.Connection;
import ru.kalinin.chat.client.connection.ConnectionConfiguration;
import ru.kalinin.chat.client.exception.ConnectNotInitializedException;
import ru.kalinin.chat.client.listener.PacketListener;
import ru.kalinin.chat.commons.model.packet.*;
import ru.kalinin.chat.commons.model.packet.Error;

/**
 * Created by kalinin on 04.10.2017.
 */
@Slf4j
public abstract class ChatClient {

    public static final int DEFAULT_PORT = 8080;

    private String host;
    private int port = DEFAULT_PORT;
    private String username;
    private ConnectionConfiguration connConfig;
    private Connection connection;

    public ChatClient(String host, int port) {
        this.host = host;
        this.port = port;
        connConfig = new ConnectionConfiguration(host, port);
        connection = new AmqpConnection(connConfig);
        init();
    }

    public void connect(String username) {
        this.username = username;
        connConfig.setUsername(username);
        connection.connect();
    }

    public void disconnect() {
        if(connection == null) {
            throw new ConnectNotInitializedException("Connect not initialized");
        }
        connection.disconnect();
    }

    private void init() {

        PacketListener messageListener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (packet instanceof Message) {
                    Message message = (Message) packet;
                    processMessage(message);
                }
            }
        };
        connection.addPacketListener(messageListener);

        PacketListener presenceListener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (packet instanceof Presence) {
                    Presence presence = (Presence) packet;
                    processPresence(presence);
                }
            }
        };
        connection.addPacketListener(presenceListener);

        PacketListener commandListener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (packet instanceof CommandResponse) {
                    CommandResponse commandResponse = (CommandResponse) packet;
                    processCommand(commandResponse);
                }

            }
        };
        connection.addPacketListener(commandListener);

        PacketListener errorListener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (packet instanceof Error) {
                    Error error = (Error) packet;
                    processError(error);
                }
            }
        };
        connection.addPacketListener(errorListener);

//        while(connection.isConnected())
//        {
//            Thread.sleep(60000);
//        }
    }

    public void addPacketListener(PacketListener packetListener) {
        if(connection == null) {
            throw new ConnectNotInitializedException();
        }
        connection.addPacketListener(packetListener);
    }

    protected abstract void processError(Error error);

    protected abstract void processCommand(CommandResponse commandResponse);


    protected abstract void processPresence(Presence presence);

    /**
     * Обработка входящего сообщения
     * @param message
     */
    protected abstract void processMessage(Message message);

    /**
     * Отправка сообщения пользователю
     * @param to
     * @param text
     */
    public void sendMessage(String to, String text) {
        if(StringUtils.hasText(text)) {
            ChatManager chatmanager = connection.getChatManager();
            Message message = chatmanager.createMessage();
            message.setBody(text);
            chatmanager.sendPacket(message);
        }
    }

    protected void sendCommand(String code, String... args) {
        if(StringUtils.hasText(code)) {
            ChatManager chatmanager = connection.getChatManager();
            Command command = chatmanager.createCommand(code, args);
            chatmanager.sendPacket(command);
        }
    }
}
