package ru.kalinin.chat.client;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.kalinin.chat.commons.model.packet.CommandResponse;
import ru.kalinin.chat.commons.model.packet.Error;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.commons.model.packet.Presence;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by kalinin on 04.10.2017.
 */
@Slf4j
public class Messager {

    @Setter
    private String username;

    private ResponseHandler handlerResponse;
    private ResponseHandler handlerResponseTerminal;
    private ResponseHandler handlerResponseError;


    private ChatClient chatClient;

    public Messager(String host, Integer port) {
        chatClient = new ChatClient(host, port) {
            @Override
            protected void processError(Error error) {
                handlerResponseError(error.getFrom(), error.getDate(), error.getBody());
            }

            @Override
            protected void processCommand(CommandResponse commandResponse) {
                handlerResponseTerminal(commandResponse.getFrom(), commandResponse.getDate(), commandResponse.getBody());
            }

            @Override
            protected void processPresence(Presence presence) {
                String response = null;
                switch (presence.getType()) {
                    case AVAILABLE:
                        response = "Пользователь \"" + presence.getFrom() + "\" присоеденился";
                        break;
                    case UNAVAILABLE:
                        response = "Пользователь \"" + presence.getFrom() + "\" вышел";
                        break;
                }
                handlerResponse(presence.getFrom(), presence.getDate(), response);
            }

            @Override
            protected void processMessage(Message message) {
                handlerResponse(message.getFrom(), message.getDate(), message.getBody());
            }
        };
    }

    public synchronized void start() {
        chatClient.connect(username);
    }

    public synchronized void start(String username ) {
        this.username = username;
        chatClient.connect(username);
    }

    public synchronized void shutdown() {
        if(chatClient != null) {
            try {
                chatClient.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void handlerResponse(String from, Date dt, String message) {
        if(handlerResponse != null) {
            handlerResponse.onResponse(from, dt, message);
        }
    }

    public void handlerResponseTerminal(String from, Date dt, String message) {
        if(handlerResponseTerminal != null) {
            handlerResponseTerminal.onResponse(from, dt, message);
        }
    }

    public void handlerResponseError(String from, Date dt, String message) {
        if(handlerResponseError != null) {
            handlerResponseError.onResponse(from, dt, message);
        }
    }

    public void setHandlerResponseMessage(ResponseHandler handler) {
        handlerResponse = handler;
    }

    public void setHandlerResponseTerminal(ResponseHandler handler) {
        handlerResponseTerminal = handler;
    }

    public void setHandlerResponseError(ResponseHandler handler) {
        handlerResponseError = handler;
    }

    public void sendMessage(String message) {
        if(message == null || message.trim().isEmpty()) {
            return;
        }
        if(isCommand(message)) {
            String[] values = extractCommand(message);
            String code = values[0];
            String[] args = Arrays.copyOfRange(values, 1, values.length);
            chatClient.sendCommand(code, args);
        } else {
            chatClient.sendMessage(null, message);
        }

    }

    public boolean isCommand(String message) {
        return message.indexOf("/") == 0;
    }

    private String[] extractCommand(String message) {
        String[] values = message.trim().split(" ");
        values[0] = values[0].substring(1);
        return values;
    }



    public static interface ResponseHandler {

        void onResponse(String from, Date dt, String message);
    }
}
