package ru.kalinin.chat.client.connection;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by kalinin on 04.10.2017.
 */
@Getter
@Setter
public class ConnectionConfiguration {

    private String host;
    private int port;
    private String username;

    public ConnectionConfiguration(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public ConnectionConfiguration(String host, int port, String username) {
        this.host = host;
        this.port = port;
        this.username = username;
    }
}
