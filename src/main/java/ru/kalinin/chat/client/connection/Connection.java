package ru.kalinin.chat.client.connection;

import ru.kalinin.chat.client.ChatManager;
import ru.kalinin.chat.client.listener.PacketListener;
import ru.kalinin.chat.commons.model.packet.Packet;
import ru.kalinin.chat.commons.model.packet.Presence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kalinin on 04.10.2017.
 */
public abstract class Connection {

    protected final ConnectionConfiguration config;

    protected final List<PacketListener> recvListeners =
            Collections.synchronizedList(new ArrayList<PacketListener>());

    protected Connection(ConnectionConfiguration configuration) {
        config = configuration;
    }
    public abstract void connect();
    public abstract boolean isConnected();
    public abstract boolean isAuthenticated();
    public abstract void addPacketListener(PacketListener packetListener);
    public abstract void sendPacket(Packet packet);
    public abstract String getUser();
    public abstract ChatManager getChatManager();
    public void disconnect() {
        disconnect(new Presence(Presence.Type.UNAVAILABLE));
    }

    public abstract void disconnect(Presence unavailablePresence);

}
