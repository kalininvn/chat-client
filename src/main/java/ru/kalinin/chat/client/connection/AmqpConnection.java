package ru.kalinin.chat.client.connection;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.kalinin.chat.client.ChatManager;
import ru.kalinin.chat.client.exception.UserIsExistsException;
import ru.kalinin.chat.client.exception.UserLongNameException;
import ru.kalinin.chat.client.exception.UserShortNameException;
import ru.kalinin.chat.client.exception.UserValidationException;
import ru.kalinin.chat.client.listener.PacketListener;
import ru.kalinin.chat.client.model.AuthUser;
import ru.kalinin.chat.client.model.UserInfo;
import ru.kalinin.chat.commons.model.packet.Packet;
import ru.kalinin.chat.commons.model.packet.Presence;

import java.io.IOException;

/**
 * Created by kalinin on 04.10.2017.
 */
@Slf4j
public class AmqpConnection extends Connection {

    private String username = null;
    private ChatManager chatManager;
    private boolean connected;
    private boolean authenticated = false;
    private SimpleMessageListenerContainer container;
    private RabbitTemplate rabbitTemplate;
    private AmqpConfiguration amqpConfiguration;
    private CachingConnectionFactory connectionFactory;
    public AmqpConnection(ConnectionConfiguration configuration) {
        super(configuration);
    }

    @Override
    public void connect() {
        if(!isConnected()) {
            initConnection();
        }
    }

    private void validationUsername() {

        if(config.getUsername() == null || config.getUsername().trim().length() == 0) {
            throw new UserValidationException("Имя не задано");
        }

        String username = config.getUsername().trim();

        if(username.length() < 3) {
            throw new UserShortNameException();
        }
        if(username.length() > 20) {
            throw new UserLongNameException();
        }
    }

    private void initConnection() {
        String host = config.getHost();
        int port = config.getPort();

        RestTemplate rt = new RestTemplate();
        ResponseEntity<UserInfo> userInfoResponse = null;

        validationUsername();

        try {
            userInfoResponse = rt.postForEntity("http://" + host + ":" + port + "/api/authenitcation/createuser"
                    , AuthUser.builder().username(config.getUsername()).build()
                    , UserInfo.class);
        }  catch (HttpClientErrorException e) {
            if(e.getStatusCode() == HttpStatus.FORBIDDEN) {
                throw new UserIsExistsException();
            } else {
                throw e;
            }
        }

        username = userInfoResponse.getBody().getUsername();

        amqpConfiguration = userInfoResponse.getBody().getAmqpConfiguration();

        connectionFactory = new CachingConnectionFactory(
                  amqpConfiguration.getProperty(AmqpConfiguration.HOSTNAME)
                , Integer.valueOf(amqpConfiguration.getProperty(AmqpConfiguration.PORT))
        );

//        connectionFactory.setChannelCheckoutTimeout()
//        connectionFactory.setConnectionTimeout(30);

        container =
                new SimpleMessageListenerContainer(connectionFactory);

        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageListener (){
            @Override
            public void onMessage(Message message) {
                Packet packet = (Packet) SerializationUtils.deserialize(message.getBody());
                for (PacketListener recvListener : recvListeners) {
                    try {
                        recvListener.processPacket(packet);
                    } catch (Exception ignore){
                        log.error("error process message", ignore);
                    } catch (Throwable ignore){
                        log.error("fatal error process message", ignore);
                    }
                }
            }
        });

        container.setMessageListener(adapter);
        container.setQueueNames(amqpConfiguration.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        container.start();

        rabbitTemplate = new RabbitTemplate(connectionFactory);
        connected = true;
        authenticated = true;
        initPresente();
    }
    private void initPresente() {
        Presence presence = new Presence(Presence.Type.AVAILABLE);
        presence.setFrom(username);
        sendPacket(presence);
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void addPacketListener(PacketListener packetListener) {
        if (packetListener == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        recvListeners.add(packetListener);
    }

    @Override
    public synchronized ChatManager getChatManager() {
        if (this.chatManager == null) {
            this.chatManager = new ChatManager(this);
        }
        return this.chatManager;

    }

    @Override
    public String getUser() {
        if (!isAuthenticated()) {
            return null;
        }
        return username;
    }

    @Override
    public void disconnect(Presence unavailablePresence) {
        shutdown(unavailablePresence);
    }

    @Override
    public void sendPacket(Packet packet) {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        }
        if (packet == null) {
            throw new NullPointerException("Packet is null.");
        }

        // TODO сделать отправку в потоке, чтобы не ждать завершения
        rabbitTemplate.convertAndSend(amqpConfiguration.getProperty(AmqpConfiguration.EXCHANGE_MESSAGE_INCOMING), amqpConfiguration.getProperty(AmqpConfiguration.ROUTING_MESSAGE_INCOMING), packet);
    }

    protected void shutdown(Presence unavailablePresence) {
        // Сообщаем серверу, что выходим
        try {
            unavailablePresence.setFrom(username);
            sendPacket(unavailablePresence);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        container.shutdown();
        if(container != null) {
            try {
                container.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // освобождаем ресурсы
        if(connectionFactory != null) {
            try {
                connectionFactory.clearConnectionListeners();
                connectionFactory.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Ждем пока все ресурсы освободятся
        try {
            Thread.sleep(500);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        authenticated = false;
        connected = false;
    }

}
